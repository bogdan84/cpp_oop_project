//
// Created by bogdan.stoica on 5/18/2018.
//

#ifndef POIECT_OOP_INTERIOR_H
#define POIECT_OOP_INTERIOR_H

class Interior {
public:
    Interior(bool);
    ~Interior();
    void setariAutomate(Termometru&, Luxmetru&, Centrala&, AerConditionat&, JaluzeleElectrice&);
};

#endif //POIECT_OOP_INTERIOR_H
