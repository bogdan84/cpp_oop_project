//
// Created by bogdan.stoica on 5/18/2018.
//

#ifndef POIECT_OOP_SENZOR_H
#define POIECT_OOP_SENZOR_H

#include <iostream>
#include <cstring>
#include <cassert>

#include "colormod.h"

using namespace std;

class Senzor {
string numeSenzor;
string tipSenzor;

public:
    void initializare(string &tip) {
        cout << tip;
        cout << " - ";
        cout << "Nume Senzor: ";
        cin >> numeSenzor;
        tipSenzor = tip;
    };
    virtual void citire() = 0;
    virtual void setCaracter() = 0;
    virtual float getValoareSenzor() = 0;
    void preiaDim(string &nume, string &tip) {
        nume = numeSenzor;
        tip = tipSenzor;
    };
};

#endif //POIECT_OOP_SENZOR_H
