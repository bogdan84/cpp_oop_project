//
// Created by bogdan.stoica on 5/18/2018.
//

#ifndef POIECT_OOP_CASAINTELIGENTA_H
#define POIECT_OOP_CASAINTELIGENTA_H

#include <iostream>
#include <cstring>
#include <cassert>

#include "Interior.cpp"
#include "Curte.cpp"

class CasaInteligenta {
protected:
    bool status;
    string numeCasa;
public:
    Interior interior(bool);
    Curte curte(bool);

    CasaInteligenta();
    ~CasaInteligenta();

    CasaInteligenta& setNumeCasa();
    CasaInteligenta& setStatus();

    string getNumeCasa();
    bool isStatus();
};

#endif //POIECT_OOP_CASAINTELIGENTA_H
