//
// Created by bogdan.stoica on 5/18/2018.
//

#include "Interior.h"

Interior::Interior(bool status) {
    cout << endl;
    cout << "INTERIOR: " << endl;

    Senzor *senzor;
    Aparat *aparat;

    string tip = "TERMOMETRU";
    Termometru termometru;
    termometru.initializare(tip);
    senzor = &termometru;
    senzor->citire();

    tip = "CENTRALA";
    Centrala centrala;
    centrala.initializare(tip);
    aparat = &centrala;
    aparat->citire();

    tip = "AER CONDITIONAT";
    AerConditionat aerConditionat;
    aerConditionat.initializare(tip);
    aparat = &aerConditionat;
    aparat->citire();

    tip = "LUXMETRU";
    Luxmetru luxmetru;
    luxmetru.initializare(tip);
    senzor = &luxmetru;
    senzor->citire();

    tip = "JALUZELE ELECTRICE";
    JaluzeleElectrice jaluzeleElectrice;
    jaluzeleElectrice.initializare(tip);
    aparat = &jaluzeleElectrice;
    aparat->citire();

    setariAutomate(termometru, luxmetru, centrala, aerConditionat, jaluzeleElectrice);

    cout << endl;
    cout << "********************** PARAMETRII SENZORI **********************" << endl;

    termometru.afisareParametrii();
    luxmetru.afisareParametrii();

    cout << endl;
    cout << "********************** PARAMETRII APARATE **********************" << endl;

    centrala.afisareParametrii();
    aerConditionat.afisareParametrii();
    jaluzeleElectrice.afisareParametrii();

    cout << endl;
}

Interior::~Interior() {

}

void Interior::setariAutomate(Termometru &termometru, Luxmetru &luxmetru, Centrala &centrala, AerConditionat &aerConditionat, JaluzeleElectrice &jaluzeleElectrice) {
    cout << endl;
    cout << "********************** SETARI AUTOMATE **********************" << endl;

    if(!centrala.status && termometru.getValoareSenzor() < 18) {
        cout << "Sunt " << termometru.getValoareSenzor() << "°C. ";
        centrala.oprestePorneste(1);
    }
    if(centrala.status && termometru.getValoareSenzor() >= 25) {
        cout << "Sunt " << termometru.getValoareSenzor() << "°C. ";
        centrala.oprestePorneste(0);
    }

    if(!aerConditionat.status && termometru.getValoareSenzor() >= 25) {
        cout << "Sunt " << termometru.getValoareSenzor() << "°C. ";
        aerConditionat.oprestePorneste(1);
    }
    if(aerConditionat.status && termometru.getValoareSenzor() < 18) {
        cout << "Sunt " << termometru.getValoareSenzor() << "°C. ";
        aerConditionat.oprestePorneste(0);
    }

    if(!jaluzeleElectrice.status && luxmetru.getValoareSenzor() <= 107) {
        cout << "Este " << luxmetru.getCaracter() << ". ";
        jaluzeleElectrice.oprestePorneste(1);
    }
    if(jaluzeleElectrice.status && luxmetru.getValoareSenzor() > 107) {
        cout << "Este " << luxmetru.getCaracter() << ". ";
        jaluzeleElectrice.oprestePorneste(0);
    }
}
