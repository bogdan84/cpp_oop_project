//
// Created by bogdan.stoica on 5/18/2018.
//

#include "Curte.h"

Curte::Curte(bool status) {
    cout << endl;
    cout << "CURTE: " << endl;

    Senzor *senzor;
    Aparat *aparat;

    string tip = "REFRACTOMETRU";
    Refractometru refractometru;
    refractometru.initializare(tip);
    senzor = &refractometru;
    senzor->citire();

    tip = "ASPERSOARE";
    Aspersoare aspersoare;
    aspersoare.initializare(tip);
    aparat = &aspersoare;
    aparat->citire();

    setariAutomate(refractometru, aspersoare);


    cout << endl;
    cout << "********************** PARAMETRII SENZORI **********************" << endl;

    refractometru.afisareParametrii();

    cout << endl;
    cout << "********************** PARAMETRII APARATE **********************" << endl;

    aspersoare.afisareParametrii();
}

Curte::~Curte() {

}

void Curte::setariAutomate(Refractometru &refractometru, Aspersoare &aspersoare) {
    cout << endl;
    cout << "********************** SETARI AUTOMATE **********************" << endl;

    if(!aspersoare.status && refractometru.getValoareSenzor() <= 30) {
        cout << "Solul este " << refractometru.getCaracter() << ". ";
        aspersoare.oprestePorneste(1);
    }
    if(aspersoare.status && refractometru.getValoareSenzor() > 70) {
        cout << "Solul este " << refractometru.getCaracter() << ". ";
        aspersoare.oprestePorneste(0);
    }
}
