//
// Created by bogdan.stoica on 5/18/2018.
//

#ifndef POIECT_OOP_APARAT_H
#define POIECT_OOP_APARAT_H

#include <iostream>
#include <cstring>
#include <cassert>

#include "colormod.h"

using namespace std;

class Aparat {
string numeAparat;
string tipAparat;

public:
    void initializare(string &tip) {
        cout << tip;
        cout << " - ";
        cout << "Nume Aparat: ";
        cin >> numeAparat;
        tipAparat = tip;
    };
    virtual void citire() = 0;
    virtual void setStatus() = 0;
    virtual void oprestePorneste(bool) = 0;
    void preiaDim(string &nume, string &tip) {
        nume = numeAparat;
        tip = tipAparat;
    };
};

#endif //POIECT_OOP_APARAT_H
