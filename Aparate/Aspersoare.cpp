//
// Created by bogdan.stoica on 5/14/2018.
//

#include "Aspersoare.h"

void Aspersoare::citire() {
    preiaDim(numeAparat, tipAparat);
    this->setStatus();
}

void Aspersoare::setStatus() {
    cout << "Seteaza status (1/0) pentru ";
    cout << tipAparat;
    cout << " (";
    cout << numeAparat;
    cout << "): ";
    cin >> status;
};

void Aspersoare::oprestePorneste(bool intrerupator) {
    if(intrerupator){
        cout << "Pornesc ";
    } else {
        cout << "Opresc ";
    }
    cout << tipAparat;
    cout << " (";
    cout << numeAparat;
    cout << ")";
    cout << endl;


    status = intrerupator;
};

void Aspersoare::afisareParametrii(){
    Color::Modifier white(Color::FG_WHITE);
    Color::Modifier blue(Color::FG_BLUE);
    Color::Modifier def(Color::FG_DEFAULT);
    Color::Modifier redBG(Color::BG_RED);
    Color::Modifier greenBG(Color::BG_GREEN);
    Color::Modifier defBG(Color::BG_DEFAULT);

    Color::Modifier colorStatus = status == 0 ? redBG : greenBG;
    string modFunctionare = status == 0 ? "Oprit" : "Pornit";

    cout << "Nume Aparat: ";
    cout << blue;
    cout << numeAparat;
    cout << def;
    cout << "\t";

    cout << "Tip Aparat: ";
    cout << tipAparat;
    cout << "\t";

    cout << "Status: ";
    cout << white;
    cout << colorStatus;
    cout << modFunctionare;
    cout << defBG;
    cout << def;
    cout << endl;
}
