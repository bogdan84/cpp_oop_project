//
// Created by bogdan.stoica on 5/18/2018.
//

#ifndef POIECT_OOP_AERCONDITIONAT_H
#define POIECT_OOP_AERCONDITIONAT_H

#include "../Aparat.h"

using namespace std;

class AerConditionat: public Aparat {
private:
    string numeAparat;
    string tipAparat;

public:
    bool status;
    void setStatus() override;
    void citire() override;
    void oprestePorneste(bool) override;
    void afisareParametrii();
};


#endif //POIECT_OOP_AERCONDITIONAT_H
