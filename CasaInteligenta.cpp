//
// Created by bogdan.stoica on 5/18/2018.
//

#include "CasaInteligenta.h"

CasaInteligenta::CasaInteligenta() {
    setNumeCasa().setStatus();

    if(isStatus()) {
        Interior interior(status);
        Curte curte(status);
    } else {
        cout << "Casa este oprita!";
    }
}

CasaInteligenta::~CasaInteligenta() {

}

CasaInteligenta& CasaInteligenta::setStatus() {
    cout << "Status (1/0): ";
    cin >> status;

    return *this;
}

string CasaInteligenta::getNumeCasa(){
    return numeCasa;
}

bool CasaInteligenta::isStatus() {
    return status;
}

CasaInteligenta& CasaInteligenta::setNumeCasa() {
    cout << "Nume Casa: ";
    cin >> numeCasa;

    return *this;
}
