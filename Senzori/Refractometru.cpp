//
// Created by bogdan.stoica on 5/14/2018.
//

#include "Refractometru.h"

void Refractometru::citire() {
    preiaDim(numeSenzorRefractometru, tipSenzorRefractometru);

    cout << "Umiditatea Gravimetrica: ";
    cin >> umiditateaGravimetrica;

    this->setCaracter();
}

void Refractometru::setCaracter() {
    if (umiditateaGravimetrica <= 30) {
        caracter = "Uscat";
    } else if (umiditateaGravimetrica > 70){
        caracter = "Umed";
    } else {
        caracter = "Neutru";
    }
};

void Refractometru::afisareParametrii(){
    Color::Modifier red(Color::FG_RED);
    Color::Modifier green(Color::FG_GREEN);
    Color::Modifier white(Color::FG_WHITE);
    Color::Modifier blue(Color::FG_BLUE);
    Color::Modifier def(Color::FG_DEFAULT);
    Color::Modifier redBG(Color::BG_RED);
    Color::Modifier greenBG(Color::BG_GREEN);
    Color::Modifier blueBG(Color::BG_BLUE);
    Color::Modifier defBG(Color::BG_DEFAULT);

    Color::Modifier colorLumeni = umiditateaGravimetrica > 70 ? blue : green;
                    colorLumeni = umiditateaGravimetrica < 30 ? red : colorLumeni;

    Color::Modifier colorStatus = umiditateaGravimetrica > 70 ? blueBG : greenBG;
                    colorStatus = umiditateaGravimetrica < 30 ? redBG : colorStatus;

    cout << "Nume Senzor: ";
    cout << blue;
    cout << numeSenzorRefractometru;
    cout << def;
    cout << "\t";

    cout << "Tip Senzor: ";
    cout << tipSenzorRefractometru;
    cout << "\t";

    cout << "Umiditatea Gravimetrica: ";
    cout << colorLumeni;
    cout << umiditateaGravimetrica;
    cout << "%";
    cout << def;
    cout << "\t";

    cout << "Caracter: ";
    cout << white;
    cout << colorStatus;
    cout << caracter;
    cout << defBG;
    cout << def;
    cout << endl;
}

float Refractometru::getValoareSenzor() {
    return umiditateaGravimetrica;
}

string Refractometru::getCaracter() {
    return caracter;
}
