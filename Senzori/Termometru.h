//
// Created by bogdan.stoica on 5/14/2018.
//

#ifndef POIECT_OOP_TERMOMETRU_H
#define POIECT_OOP_TERMOMETRU_H

#include "../Senzor.h"

using namespace std;

class Termometru: public Senzor {
private:
    float temperatura = 15.00;

    string caracter;
    string numeSenzorTermometru;
    string tipSenzorTermometru;

public:
    void setCaracter() override;
    void citire() override;
    float  getValoareSenzor() override;
    void afisareParametrii();
    string getCaracter();
};

#endif //POIECT_OOP_TERMOMETRU_H
