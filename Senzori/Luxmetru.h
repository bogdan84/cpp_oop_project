//
// Created by bogdan.stoica on 5/14/2018.
//

#ifndef POIECT_OOP_LUXMETRU_H
#define POIECT_OOP_LUXMETRU_H

#include "../Senzor.h"

using namespace std;

class Luxmetru: public Senzor {
private:
    float lumeni = 25.00;

    string caracter;
    string numeSenzorLuxmetru;
    string tipSenzorLuxmetru;

public:
    void setCaracter() override;
    void citire() override;
    float getValoareSenzor() override;
    void afisareParametrii();
    string getCaracter();
};

#endif //POIECT_OOP_LUXMETRU_H
