//
// Created by bogdan.stoica on 5/14/2018.
//

#include "Luxmetru.h"

void Luxmetru::citire() {
    preiaDim(numeSenzorLuxmetru, tipSenzorLuxmetru);

    cout << "Numar Lumeni: ";
    cin >> lumeni;

    this->setCaracter();
}

void Luxmetru::setCaracter() {
    if (lumeni <= 107) {
        caracter = "Noapte";
    } else {
        caracter = "Zi";
    }
};

void Luxmetru::afisareParametrii(){
    Color::Modifier red(Color::FG_RED);
    Color::Modifier green(Color::FG_GREEN);
    Color::Modifier white(Color::FG_WHITE);
    Color::Modifier blue(Color::FG_BLUE);
    Color::Modifier def(Color::FG_DEFAULT);
    Color::Modifier redBG(Color::BG_RED);
    Color::Modifier greenBG(Color::BG_GREEN);
    Color::Modifier defBG(Color::BG_DEFAULT);

    Color::Modifier colorLumeni = lumeni <= 107 ? red : green;
    Color::Modifier colorStatus = lumeni <= 107 ? redBG : greenBG;

    cout << "Nume Senzor: ";
    cout << blue;
    cout << numeSenzorLuxmetru;
    cout << def;
    cout << "\t";

    cout << "Tip Senzor: ";
    cout << tipSenzorLuxmetru;
    cout << "\t";

    cout << "Cantitate lumeni ";
    cout << colorLumeni;
    cout << lumeni;
    cout << def;
    cout << "\t";

    cout << "Caracter: ";
    cout << white;
    cout << colorStatus;
    cout << caracter;
    cout << defBG;
    cout << def;
    cout << endl;
}

float Luxmetru::getValoareSenzor() {
    return lumeni;
}

string Luxmetru::getCaracter() {
    return caracter;
}
