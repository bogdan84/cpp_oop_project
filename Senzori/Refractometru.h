//
// Created by bogdan.stoica on 5/14/2018.
//

#ifndef POIECT_OOP_REFRACTOMETRU_H
#define POIECT_OOP_REFRACTOMETRU_H

#include "../Senzor.h"

using namespace std;

class Refractometru: public Senzor {
private:
    float umiditateaGravimetrica = 25.00;

    string caracter;
    string numeSenzorRefractometru;
    string tipSenzorRefractometru;

public:
    void setCaracter() override;
    void citire() override;
    float getValoareSenzor() override;
    void afisareParametrii();
    string getCaracter();
};

#endif //POIECT_OOP_REFRACTOMETRU_H
