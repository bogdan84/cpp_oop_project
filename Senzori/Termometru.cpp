//
// Created by bogdan.stoica on 5/14/2018.
//

#include "Termometru.h"

void Termometru::citire() {
    preiaDim(numeSenzorTermometru, tipSenzorTermometru);

    cout << "Temperatura: ";
    cin >> temperatura;

    this->setCaracter();
}

void Termometru::setCaracter() {
    if (temperatura < 18) {
        caracter = "Frig";
    } else if (temperatura >= 25){
        caracter = "Cald";
    } else {
        caracter = "Neutru";
    }
};

void Termometru::afisareParametrii(){
    Color::Modifier red(Color::FG_RED);
    Color::Modifier green(Color::FG_GREEN);
    Color::Modifier white(Color::FG_WHITE);
    Color::Modifier blue(Color::FG_BLUE);
    Color::Modifier def(Color::FG_DEFAULT);
    Color::Modifier redBG(Color::BG_RED);
    Color::Modifier greenBG(Color::BG_GREEN);
    Color::Modifier blueBG(Color::BG_BLUE);
    Color::Modifier defBG(Color::BG_DEFAULT);

    Color::Modifier colorTempInt = temperatura >= 25 ? red : green;
                    colorTempInt = temperatura < 18 ? blue : colorTempInt;
    Color::Modifier colorStatus =  temperatura >= 25 ? redBG : greenBG;
                    colorStatus = temperatura < 18 ? blueBG : colorStatus;

    cout << "Nume Senzor: ";
    cout << blue;
    cout << numeSenzorTermometru;
    cout << def;
    cout << "\t";

    cout << "Tip Senzor: ";
    cout << tipSenzorTermometru;
    cout << "\t";

    cout << "Temperatura: ";
    cout << colorTempInt;
    cout << temperatura;
    cout << "°C";
    cout << def;
    cout << "\t";

    cout << "Caracter: ";
    cout << white;
    cout << colorStatus;
    cout << caracter;
    cout << defBG;
    cout << def;
    cout << endl;
}

float  Termometru::getValoareSenzor() {
    return temperatura;
}

string Termometru::getCaracter() {
    return caracter;
}
