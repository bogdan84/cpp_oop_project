#include "Senzori/Termometru.h"
#include "Senzori/Luxmetru.h"
#include "Senzori/Refractometru.h"

#include "Aparate/Centrala.h"
#include "Aparate/AerConditionat.h"
#include "Aparate/JaluzeleElectrice.h"
#include "Aparate/Aspersoare.h"

#include "CasaInteligenta.cpp"


//class Interior;

int main() {

    #ifdef WINDOWS
        system("cls");
    #endif
    #ifdef LINUX
        system("clear");
    #endif

    CasaInteligenta casaInteligenta;

    return EXIT_SUCCESS;
}