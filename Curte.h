//
// Created by bogdan.stoica on 5/18/2018.
//

#ifndef POIECT_OOP_AFARA_H
#define POIECT_OOP_AFARA_H

class Curte {
public:
    Curte(bool);
    ~Curte();
    void setariAutomate(Refractometru&, Aspersoare&);
};


#endif //POIECT_OOP_AFARA_H
